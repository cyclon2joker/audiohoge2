//
//  GLHogeView.h
//  AudioHoge
//
//  Created by pies on 2018/01/27.
//  Copyright © 2018年 pies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BufferManager.h"

@interface GLHogeView : UIView

@property (nonatomic) BOOL applicationResignedActive;

@property (nonatomic,weak) BufferManager *bm;

- (IBAction)modeGlView:(id)sender;

@property (nonatomic) BOOL modeSignWave;

- (void)startAnimation;

- (void)drawAudioData:(Float32*)audioData;

@end
