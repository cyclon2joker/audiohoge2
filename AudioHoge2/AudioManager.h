//
//  AudioManager.h
//  AudioHoge2
//
//  Created by pies on 2018/02/10.
//  Copyright © 2018年 pies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>


@interface AudioManager : NSObject


-(AVAudioPCMBuffer*)loadAudioDataFromResource:(NSString*)resourceName;





@end
