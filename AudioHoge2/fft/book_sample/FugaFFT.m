//
//  FugaFFT.m
//  AudioHoge3
//
//  Created by pies on 2018/02/17.
//  Copyright © 2018年 pies. All rights reserved.
//

#import <Accelerate/Accelerate.h>

#import "FugaFFT.h"


@interface FugaFFT() {

    NSInteger frameSize;
    NSInteger sampleRate;
    
    NSInteger fftSize;
    vDSP_Length log2n;
    FFTSetup fftsetup;
    Float32* window;
    DSPSplitComplex splitComplex;

    Float32 *results;

//    FFTSetup fftSetup;
    unsigned int capacity;
    unsigned int capacityN;    //capacityが2の何乗であるかを保持
    Float32* windowedInput;

    
    
}


@end


@implementation FugaFFT


- (id)initWithFrameSize:(NSInteger)fsz sampleR:(NSInteger)sampleR
{
    self = [super init];
    if (self) {
        frameSize = fsz;
        sampleRate = sampleR;
        fftSize = frameSize * 2;
        results = calloc(frameSize, sizeof(Float32));
        
        log2n = log2(fftSize);

        fftsetup = vDSP_create_fftsetup(log2n, FFT_RADIX2);
        // 窓 初期化
        window = calloc(fftSize, sizeof(Float32));
        vDSP_hann_window(window, fftSize, 0);
        // 実数、虚数配列 初期化
        splitComplex.realp = calloc(frameSize, sizeof(Float32));
        splitComplex.imagp = calloc(frameSize, sizeof(Float32));
        
//        capacityN = log(frameSize) / log(2);
//        capacity = 1 << capacityN;
//        NSLog(@"capacity: %d n: %d", capacity, capacityN);
//
//        // FFTの設定をします
//        fftSetup = vDSP_create_fftsetup(capacityN+1, );
//        // FFTに使う配列を用意します
//        splitComplex.realp = calloc(capacity, sizeof(Float32));
//        splitComplex.imagp = calloc(capacity, sizeof(Float32));
//        // 窓用の配列を用意します
//        windowedInput = calloc(frameSize * 2, sizeof(Float32));
//        // 窓を作ります
//        vDSP_hann_window(window, capacity, 0);

    }
    return self;
}

-(Float32*)calc:(Float32*)audioData
{
    Float32 *aBuffer = calloc(fftSize, sizeof(Float32));
    for (int i = 0; i < fftSize; i++) {
        aBuffer[i] = audioData[i];
    }
    
    //hanning window
    //windowing
    vDSP_vmul(aBuffer, 1, window, 1, aBuffer, 1, fftSize);
    //transform to complex
    vDSP_ctoz((COMPLEX *)aBuffer, 2, &splitComplex, 1, frameSize);
    //FFT forward
    vDSP_fft_zrip(fftsetup, &splitComplex, 1, log2n, FFT_FORWARD);
    
//    //scaling

    Float32 scale = 0.1;//1.0/(fftSize*2);
    vDSP_vsmul(splitComplex.realp, 1, &scale, splitComplex.realp, 1, frameSize);
    vDSP_vsmul(splitComplex.imagp, 1, &scale, splitComplex.imagp, 1, frameSize);

    vDSP_zvabs(&splitComplex, 1, results, 1, frameSize);
    
//    vDSP_vdist(splitComplex.realp, 1, splitComplex.imagp, 1, results, 1, frameSize);
    return results;
}

- (double)getFrequencyRate
{
    return (double)(sampleRate / fftSize);
}




//- (Float32*)process:(Float32*)input
//{
//    // 窓をかけます
//    vDSP_vmul(input, 1, window, 1, windowedInput, 1, capacity);
//
//    // 複素数に変換します
//    for (int i=0; i<capacity; i++) {
//        splitComplex.realp[i] = windowedInput[i];
//        splitComplex.imagp[i] = 0.0f;
//    }
//
//    // フーリエ変換します
//    vDSP_fft_zrip(fftSetup, &splitComplex, 1, capacityN+1, FFT_FORWARD);
//    Float32* results = calloc(capacity/2, sizeof(Float32));
//
//    vDSP_vdist([self realp], 1, [self imagp], 1, results, 1, capacity/2);
//
//    for (int i = 0; i < capacity; i++) {
//
//        printf("adt[%d],%fhz,%f\n", i,_sampleRate * i / capacity, (Float32)results[i] );
//    }
//
//
//    return results;
//}





@end
