//
//  FooFFT.m
//  AudioHoge2
//
//  Created by pies on 2018/02/15.
//  Copyright © 2018年 pies. All rights reserved.
//
#import <Accelerate/Accelerate.h>
#import "FooFFT.h"

@interface FooFFT() {
    Float32 *results;
    DSPSplitComplex splitComplex;
    FFTSetup fftSetup;
    unsigned int capacity;
    unsigned int capacityN;    //capacityが2の何乗であるかを保持
    Float32* window;
    Float32* windowedInput;

    
}

@end
@implementation FooFFT


- (Float32*)doFFT:(Float32*)audioData frameSize:(int)frameSize {
    
    results = calloc(frameSize, sizeof(Float32));
//    for (int i = 0; i < frameSize * 2; i++) {
//        results[i] = audioData[i];
//    }
    [self calcFFT:results dataLength:frameSize * 2];
    return results;
}


// data: 入力
// length: 入力のうち利用する要素数
- (void)calcFFT:(float*)data dataLength:(int)length
{
    // データ長を2のn乗の大きさにする
    unsigned int sizeLog2 = (int)(log(length)/log(2));
    unsigned int size = 1 << sizeLog2;
    
    // fftのセットアップ
    FFTSetup fftSetUp = vDSP_create_fftsetup(sizeLog2 + 1, FFT_RADIX2);
    
    // 窓関数の用意
    Float32* window = calloc(size, sizeof(Float32));
    Float32* windowedInput = calloc(size, sizeof(Float32));
    vDSP_hann_window(window, size, 0);
    
    // 窓関数を入力値に適用し、windewedInputへ
    vDSP_vmul(data, 1, window, 1, windowedInput, 1, size);
    
    // 入力を複素数にする
    DSPSplitComplex splitComplex;
    splitComplex.realp = calloc(size, sizeof(Float32));
    splitComplex.imagp = calloc(size, sizeof(Float32));
    
    for (int i = 0; i < size; i++) {
        splitComplex.realp[i] = windowedInput[i];
        splitComplex.imagp[i] = 0.0f;
    }
    
    // FFTを計算する
    vDSP_fft_zrip(fftSetUp, &splitComplex, 1, sizeLog2 + 1, FFT_FORWARD);
    
    // 結果を表示する
    // FFTの性質から半分のデータのみ利用する
//    for (int i = 0; i <= size/2; i++) {
//        Float32 real = splitComplex.realp[i];
//        Float32 imag = splitComplex.imagp[i];
//        Float32 distance = sqrt(real*real + imag*imag);
//        results[i] = distance;
////        NSLog(@"[%d] %.2f", i, distance);
//    }
    
    vDSP_vdist(splitComplex.realp, 1, splitComplex.imagp, 1, results, 1, size / 2);
    
    // メモリを開放する
    free(splitComplex.realp);
    free(splitComplex.imagp);
    free(window);
    free(windowedInput);
    vDSP_destroy_fftsetup(fftSetUp);
    
}


- (float*)realp
{
    return splitComplex.realp;
}
- (float*)imagp
{
    return splitComplex.imagp;
}

- (id)initWithCapacity:(unsigned int)aCapacity
{
    if (self = [super init]) {
        
        // aCapacityが2のn乗になっているか調べます
        // aCapacityが2のn乗になっていない場合は、
        // 2のn乗になるように調整します。
        // (厳密にやりたい場合は0のチェック等も行ってください)
        capacityN = log(aCapacity) / log(2);
        capacity = 1 << capacityN;
        
        NSLog(@"capacity: %d n: %d", capacity, capacityN);
        
        // FFTの設定をします
        fftSetup = vDSP_create_fftsetup(capacityN+1, FFT_RADIX2);
        
        // FFTに使う配列を用意します
        splitComplex.realp = calloc(capacity, sizeof(Float32));
        splitComplex.imagp = calloc(capacity, sizeof(Float32));
        
        // 窓用の配列を用意します
        window = calloc(capacity, sizeof(Float32));
        windowedInput = calloc(capacity, sizeof(Float32));
        
        // 窓を作ります
        vDSP_hann_window(window, capacity, 0);
    }
    return self;
}

- (Float32*)process:(Float32*)input
{
    // 窓をかけます
    vDSP_vmul(input, 1, window, 1, windowedInput, 1, capacity);
    
    // 複素数に変換します
    for (int i=0; i<capacity; i++) {
        splitComplex.realp[i] = windowedInput[i];
        splitComplex.imagp[i] = 0.0f;
    }
    
    // フーリエ変換します
    vDSP_fft_zrip(fftSetup, &splitComplex, 1, capacityN+1, FFT_FORWARD);
    Float32* results = calloc(capacity/2, sizeof(Float32));

    vDSP_vdist([self realp], 1, [self imagp], 1, results, 1, capacity/2);
    
    for (int i = 0; i < capacity; i++) {
        
        printf("adt[%d],%fhz,%f\n", i,_sampleRate * i / capacity, (Float32)results[i] );
    }
    
    
    return results;
}

- (void)dealloc
{
    // FFTに使う配列を解放します
    free(splitComplex.realp);
    free(splitComplex.imagp);
    
    // 窓用の配列を解放します
    free(window);
    free(windowedInput);
    
    // FFTの設定を削除します
    vDSP_destroy_fftsetup(fftSetup);
//    [super dealloc];
}



@end
