//
//  FugaFFT.h
//  AudioHoge3
//
//  Created by pies on 2018/02/17.
//  Copyright © 2018年 pies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FugaFFT : NSObject

- (id)initWithFrameSize:(NSInteger)fsz sampleR:(NSInteger)sampleR;

-(Float32*)calc:(Float32*)audioData;

- (double)getFrequencyRate;

@end
