//
//  BufferManager.h
//  AudioHoge
//
//  Created by pies on 2018/02/04.
//  Copyright © 2018年 pies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BufferManager : NSObject

-(void)copyAudioDataToDrawBuffer:(Float32*)inData frame:(UInt32)frame;

- (Float32*)getDrawBuffer;

@end
