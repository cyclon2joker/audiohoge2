//
//  ViewController.m
//  AudioHoge2
//
//  Created by pies on 2018/02/10.
//  Copyright © 2018年 pies. All rights reserved.
//

#import "ViewController.h"
#import "AudioManager.h"
#import "GLHogeView.h"
#import "FugaFFT.h"
#import "FooFFT.h"

@interface ViewController () {

    NSString *sampleAudio;
    
    AudioManager *manager;
    FugaFFT *fft;
//    FooFFT *fft2;
//    FooFFT *fft3;

    BOOL isFirstViewAppear;
    
    
}

@property (weak, nonatomic) IBOutlet UISegmentedControl *modeGLView;

@property (nonatomic) GLHogeView *glView;
@property (weak, nonatomic) IBOutlet UIView *containerV;

@property (weak, nonatomic) IBOutlet UILabel *lblAudioInfo;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segSampleTp;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isFirstViewAppear = YES;
    _glView = nil;
    
    sampleAudio = @"sample_440_5";
    manager = [AudioManager new];
    fft = [[FugaFFT alloc] initWithFrameSize:1024 sampleR:44100];
    
//    fft2 = [FooFFT new];
//    fft3 = [[FooFFT alloc] initWithCapacity:2048];
    
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (isFirstViewAppear) {
        isFirstViewAppear = NO;
        [self setupGLView];
    }
}


- (void)setupGLView {
    
    CGRect rct = _containerV.frame;
    rct.origin = CGPointZero;
    _glView = [[GLHogeView alloc] initWithFrame:rct];
    [_containerV addSubview:_glView];
    
}
- (IBAction)changeMode:(id)sender {
    
    _glView.modeSignWave = _modeGLView.selectedSegmentIndex == 0;
    
}


- (IBAction)tapLoad:(id)sender {

    AVAudioPCMBuffer *buffer = [manager loadAudioDataFromResource:sampleAudio];
    
    NSLog(@"frame len:%f", buffer.frameLength);

    _lblAudioInfo.text = [NSString stringWithFormat:@"sampleRate:%d,channels:%d,sz:%u",
                          (int)buffer.format.sampleRate, buffer.format.channelCount,buffer.frameCapacity];
    
    // 1フレーム:1024hz サンプルレート:44.1kHz -> (1 / 44100) * (44100 / 1024) -> 0.0009???
//    buffer.frameLength = 1024;
//    Float32 *audioData = buffer.audioBufferList->mBuffers[0].mData;
//    [_glView drawAudioData:audioData];
    
    //
    
//    [_glView drawAudioData:buffer.audioBufferList->mBuffers[0].mData];
    
    Float32 *audioData = buffer.floatChannelData[0];
    
    if (_modeGLView.selectedSegmentIndex == 0) {
        [_glView drawAudioData:audioData];
    } else {

        Float32 *fftData = [fft calc:audioData];
        
//        float vdist[1024];
//        vDSP_vdist([fft3 realp], 1, [fft3 imagp], 1, vdist, 1, 1024);
        
//        Float32 *fftData = [fft FFT:buffer.floatChannelData[0] frameSize:1024];
        [_glView drawAudioData:fftData];

    }

    //    try self.audioFile.readIntoBuffer(self.PCMBuffer)
    //
    //    //各チャンネル毎にバイナリデータをbufferに追加する
    //    for i in 0..<self.nChannel!{
    //
    //        let buf:[Float] = Array(UnsafeMutableBufferPointer(start:self.PCMBuffer.floatChannelData[i], count:self.nframe!))
    //
    //        self.buffer.append(buf)
    //
    //    }

    
    

}


- (IBAction)changeSample:(id)sender {
    NSInteger tp = _segSampleTp.selectedSegmentIndex;
    switch (tp) {
        case 0:
        sampleAudio = @"sample_440_5";
        break;
        case 1:
        sampleAudio = @"sample_440_261_5";
        break;
        case 2:
        sampleAudio = @"sample_440_660_5";
        break;

        default:
        sampleAudio = @"sample_261_660_5";
        break;
    }
}



@end
