//
//  main.m
//  AudioHoge2
//
//  Created by pies on 2018/02/10.
//  Copyright © 2018年 pies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
