//
//  AudioManager.m
//  AudioHoge2
//
//  Created by pies on 2018/02/10.
//  Copyright © 2018年 pies. All rights reserved.
//

#import "AudioManager.h"

@interface AudioManager() {
    AVAudioSession          *session;
    AVAudioEngine           *_engine;
    AVAudioUnitSampler      *_sampler;
    AVAudioUnitDistortion   *_distortion;
    AVAudioUnitReverb       *_reverb;
    AVAudioPlayerNode       *_player;
    
    // the sequencer
    AVAudioSequencer        *_sequencer;
    double                  _sequencerTrackLengthSeconds;
    
    // buffer for the player
    AVAudioPCMBuffer        *_playerLoopBuffer;
    
    // for the node tap
    NSURL                   *_mixerOutputFileURL;
    BOOL                    _isRecording;
    BOOL                    _isRecordingSelected;
    
    // mananging session and configuration changes
    BOOL                    _isSessionInterrupted;
    BOOL                    _isConfigChangePending;

    
}

@end


@implementation AudioManager


-(id)init {
    self = [super init];
    [self setupAudioSession];
    
    return self;
}

- (void)setupAudioSession {
    
    // Configure the audio session
    session = [AVAudioSession sharedInstance];
    NSError *error;
    
    // set the session category
    bool success = [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionMixWithOthers error:&error];
    if (!success) NSLog(@"Error setting AVAudioSession category! %@\n", [error localizedDescription]);
    
    // サンプルレート:44.1 kHz
    double hwSampleRate = 44100.0;
    success = [session setPreferredSampleRate:hwSampleRate error:&error];
    if (!success) NSLog(@"Error setting preferred sample rate! %@\n", [error localizedDescription]);
    
    NSTimeInterval ioBufferDuration = 0.0029;
    success = [session setPreferredIOBufferDuration:ioBufferDuration error:&error];
    if (!success) NSLog(@"Error setting preferred io buffer duration! %@\n", [error localizedDescription]);
    
    // 計測モードにする
    [session setMode:AVAudioSessionModeMeasurement error:&error];
    
    // 割り込み系のコールバックはとりあえず実装しない
    // activate the audio session

    success = [session setActive:YES error:&error];
    if (!success) NSLog(@"Error setting session active! %@\n", [error localizedDescription]);
}


- (void)createEngineAndAttachNodes
{
    NSError *error;
    BOOL success = NO;
    
    _engine = nil;
    _sampler = nil;
    _distortion = nil;
    _reverb = nil;
    _player = nil;
    
    
    _player = [[AVAudioPlayerNode alloc] init];
    
    _sampler = [[AVAudioUnitSampler alloc] init];
    
    
    //    // load drumloop into a buffer for the playernode
    //    NSURL *drumLoopURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"drumLoop" ofType:@"caf"]];
    //    AVAudioFile *drumLoopFile = [[AVAudioFile alloc] initForReading:drumLoopURL error:&error];
    //    _playerLoopBuffer = [[AVAudioPCMBuffer alloc] initWithPCMFormat:[drumLoopFile processingFormat] frameCapacity:(AVAudioFrameCount)[drumLoopFile length]];
    //    success = [drumLoopFile readIntoBuffer:_playerLoopBuffer error:&error];
    //    NSAssert(success, @"couldn't read drumLoopFile into buffer, %@", [error localizedDescription]);
    
    _isRecording = NO;
    _isRecordingSelected = NO;
    
    _engine = [AVAudioEngine new];
    
    AVAudioMixerNode *mixer = [_engine mainMixerNode];
    AVAudioInputNode *input = [_engine inputNode];
    
    
    [_engine connect:input to:mixer format:[input inputFormatForBus:0]];
    
    
//    __block __weak typeof(self) wSelf = self;
//
//
//    [input installTapOnBus:0 bufferSize:1024 format:[input outputFormatForBus:0] block:^(AVAudioPCMBuffer *buffer, AVAudioTime *when) {
//
//        // 1フレーム:1024hz サンプルレート:44.1kHz -> (1 / 44100) * (44100 / 1024) -> 0.0009???
//        buffer.frameLength = 1024;
//        Float32 *audioData = buffer.audioBufferList->mBuffers[0].mData;
//
//        if (audioData != NULL) {
//            [wSelf.bm copyAudioDataToDrawBuffer:audioData frame:buffer.frameLength];
//            for (UInt32 i=0; i<buffer.frameLength; i++) {
//                NSLog(@"indata[%d]:%f\n",i,audioData[i]);
//            }
//        }
//
//    }];
    
    [_engine startAndReturnError:&error];
    
    
    /*  To support the instantiation of arbitrary AVAudioNode subclasses, instances are created
     externally to the engine, but are not usable until they are attached to the engine via
     the attachNode method. */
    
    //    [_engine attachNode:_sampler];
    //    [_engine attachNode:_distortion];
    //    [_engine attachNode:_reverb];
    //    [_engine attachNode:_player];
    //    [_engine st]
    
}

-(AVAudioPCMBuffer*)loadAudioDataFromResource:(NSString*)resourceName {
    
    NSString *path = [[NSBundle mainBundle] pathForResource:resourceName ofType:@"wav"];
    
    NSURL *url = [[NSURL alloc] initFileURLWithPath:path];
    
    NSError *err = nil;
    AVAudioFile *fp = [[AVAudioFile alloc] initForReading:url error:&err];
    if (err) {
        NSLog(@"File Load Error(%@) cause:%@\n",path ,[err localizedDescription]);
        return nil;
    }

    //get samplingRate
    Float32 sampleRate = fp.fileFormat.sampleRate;
    //get channel
    NSUInteger channcelCnt = fp.fileFormat.channelCount;    
    NSUInteger length = fp.length;
    
    NSLog(@"Load success(%@), SamplingRate:%f, channel:%lu, size:%u,%u,%u",
          path, sampleRate, channcelCnt, length, length/1024,length/sampleRate);

    AVAudioPCMBuffer *pcmBuffer = [[AVAudioPCMBuffer alloc] initWithPCMFormat:fp.processingFormat frameCapacity:fp.length];
    
    [fp readIntoBuffer:pcmBuffer error:&err];
    if (err) {
        NSLog(@"%@",err.localizedDescription);
        return nil;
    }
    return pcmBuffer;
}





@end
