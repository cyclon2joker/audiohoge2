//
//  AppDelegate.h
//  AudioHoge2
//
//  Created by pies on 2018/02/10.
//  Copyright © 2018年 pies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

